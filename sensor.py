import sensorlib
import RPi.GPIO as GPIO
import time
import signal


def signal_handler(signaltype, frame):
    GPIO.cleanup()


def main():
    # bind killsignal
    signal.signal(signal.SIGINT, signal_handler)

    sensorlib.init(1, 0x40)
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(11, GPIO.OUT)
    GPIO.setup(12, GPIO.OUT)

    # initial selection
    GPIO.output(11, GPIO.LOW)
    GPIO.output(12, GPIO.LOW)

    while True:
        GPIO.output(11, GPIO.HIGH)
        time.sleep(1)
        humidity = sensorlib.read_humidity()
        temperature = sensorlib.read_temperature()
        print('1 Humidity:    ' + repr(humidity))
        print('1 Temperature: ' + repr(temperature))
        GPIO.output(11, GPIO.LOW)

        GPIO.output(12, GPIO.HIGH)
        time.sleep(1)
        humidity = sensorlib.read_humidity()
        temperature = sensorlib.read_temperature()
        print('2 Humidity:    ' + repr(humidity))
        print('2 Temperature: ' + repr(temperature))
        GPIO.output(12, GPIO.LOW)


if __name__ == "__main__":
    main()